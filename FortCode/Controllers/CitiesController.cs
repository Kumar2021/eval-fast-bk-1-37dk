﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Data.Entities;
using FortCode.Data.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public CitiesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IEnumerable<Cities> GetCities()
        {
            return _unitOfWork.CityRepository.GetAll();
        }
        [HttpPost]
        public bool Add(Cities city)
        {
            _unitOfWork.CityRepository.Save(city);
            return _unitOfWork.Complete();
        }
        [HttpDelete("{id}")]
        public bool Remove(int id)
        {
            _unitOfWork.CityRepository.Remove(id);
            return _unitOfWork.Complete();
        }
    }
}
