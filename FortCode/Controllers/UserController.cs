﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Data;
using FortCode.Data.Entities;
using FortCode.Data.Models;
using FortCode.Data.Repository;
using FortCode.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly DataContext _datContext;
        private readonly ITokenService _tokenService;
        private readonly IUnitOfWork _unitOfWork;
        public UserController(DataContext dataContext, ITokenService tokenService,IUnitOfWork unitOfWork)
        {
            _datContext = dataContext;
            _tokenService = tokenService;
            _unitOfWork = unitOfWork;
        }
        [Route("register")]
        [HttpPost]
        public bool Register(Users user)
        {
            _unitOfWork.UserRepository.Save(user);
            return _unitOfWork.Complete();
        }

        [Route("login")]
        [HttpPost]
        public IActionResult Login(LoginModel credentials)
        {
            var data = _unitOfWork.UserRepository.GetUser(credentials.Name, credentials.Password);

            if (data != null)
            {
                return Ok(_tokenService.CreateToken(credentials.Name));
            }
            return  BadRequest(new { message = "Username or password is incorrect, please provide valid credentials" });

        }

    }
}
