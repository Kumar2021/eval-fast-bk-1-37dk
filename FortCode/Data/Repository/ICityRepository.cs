﻿using FortCode.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Data.Repository
{
    public interface ICityRepository
    {
        IEnumerable<Cities> GetAll();
        void Save(Cities entity);
        void Remove(int Id);
    }
}
