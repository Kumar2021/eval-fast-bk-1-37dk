﻿using FortCode.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Data.Repository
{
    public class UserRepository: IUserRepository
    {
        private readonly DataContext _dataContext;
        public UserRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Save(Users user)
        {
            _dataContext.Add(user);
        }

        public Users GetUser(string name,string password)
        {
            return _dataContext.Users.SingleOrDefault(x => x.Name.Equals(name) && x.Password.Equals(password));
        }
    }
}
