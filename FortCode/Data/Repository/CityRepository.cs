﻿using FortCode.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Data.Repository
{
    public class CityRepository : ICityRepository
    {
        private readonly DataContext _dataContext;
        public CityRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IEnumerable<Cities> GetAll()
        {
            return _dataContext.Cities.ToList();
        }

        public void Remove(int Id)
        {
            var city = _dataContext.Cities.Find(Id);
            if(city!=null)
            _dataContext.Remove(city);

        }

        public void Save(Cities city)
        {
            _dataContext.Add(city);
        }
    }
}
